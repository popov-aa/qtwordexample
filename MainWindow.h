#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <ActiveQt/QAxWidget>

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{

    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_actionFileOpen_triggered();
    void on_actionInsertTable_triggered();
    void on_actionInsertText_triggered();
    void on_actionInsertParagraph_triggered();
    void on_actionInsertPage_triggered();
    void on_actionInsertSomething_triggered();

private:

    Ui::MainWindow * ui;
    QAxWidget * m_axWidget;

};

#endif // MAINWINDOW_H
