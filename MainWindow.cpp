#include "MainWindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QApplication>
#include <QAxObject>
#include <QInputDialog>
#include <QDebug>
#include <limits>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_axWidget = new QAxWidget("Word.Document", this);
    setCentralWidget(m_axWidget);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionFileOpen_triggered()
{
    QString filepath = QFileDialog::getOpenFileName(this, tr("Open document"), QApplication::applicationDirPath(), "*.docx");
    if (!filepath.isEmpty())
    {
        m_axWidget->setControl(filepath);
    }
}

void MainWindow::on_actionInsertTable_triggered()
{
    bool ok = false;

    int rowCount = QInputDialog::getInt(this, tr("Input"), tr("Row count:"), 1, 1, INT_MAX, 1, &ok);
    if (!ok) return;

    int columnCount = QInputDialog::getInt(this, tr("Input"), tr("Column count:"), 1, 1, INT_MAX, 1, &ok);
    if (!ok) return;

    QAxObject * activeWindow = m_axWidget->querySubObject( "ActiveWindow" );
    QAxObject * selection = activeWindow->querySubObject( "Selection" );
    QAxObject * range = selection->querySubObject("Range()");
    QAxObject * tables = m_axWidget->querySubObject("Tables()");
    int autoFitBehavior = 2; // 1 - wdAutoFitContent, 2 - wdAutoFitWindow, 3 - wdAutoFitFixed

    QAxObject * newTable = tables->querySubObject("Add(Range, NumRows, NumColumns, DefaultTableBehavior, AutoFitBehavior)", range->asVariant(), rowCount, columnCount, 1, autoFitBehavior);

    for (int i = 1; i <= rowCount; ++i)
    {
        for (int j = 1; j <= columnCount; ++j)
        {
            QAxObject * cell = newTable->querySubObject("Cell(Row, Column)", i, j);
            QAxObject * cellRange = cell->querySubObject("Range()");
            cellRange->dynamicCall("InsertAfter(Text)", QString("Cell %1:%2").arg(i).arg(j));
        }
    }
    selection->dynamicCall("MoveEnd(Unit, Count)", 10, rowCount);
    selection->dynamicCall( "Collapse(int)", 0);
}

void MainWindow::on_actionInsertText_triggered()
{
    QString text = QInputDialog::getText(this, tr("Input"), tr("Text:"));
    if (!text.isEmpty())
    {
        QAxObject * activeWindow = m_axWidget->querySubObject( "ActiveWindow" );
        QAxObject * selection = activeWindow->querySubObject( "Selection" );
        selection->dynamicCall("TypeText(const QString&)", text);
    }
}

void MainWindow::on_actionInsertParagraph_triggered()
{
    QAxObject * activeWindow = m_axWidget->querySubObject( "ActiveWindow" );
    QAxObject * selection = activeWindow->querySubObject( "Selection" );
    selection->dynamicCall("TypeParagraph()");
}

void MainWindow::on_actionInsertPage_triggered()
{
    QAxObject * activeWindow = m_axWidget->querySubObject( "ActiveWindow" );
    QAxObject * selection = activeWindow->querySubObject( "Selection" );
    selection->dynamicCall("Collapse(int)", 0);
    selection->dynamicCall("InsertNewPage()");
    selection->dynamicCall("Collapse(int)", 0);
}

void MainWindow::on_actionInsertSomething_triggered()
{
    QAxObject * activeWindow = m_axWidget->querySubObject( "ActiveWindow" );
    QAxObject * selection = activeWindow->querySubObject( "Selection" );
    selection->dynamicCall("InsertCaption(Text)", "Label");
}
